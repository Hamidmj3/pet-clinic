package bdd;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.owner.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class PetServiceFeature {

	@Autowired
	PetService petService;

	@Autowired
	OwnerRepository ownerRepository;

	@Autowired
	PetRepository petRepository;

	@Autowired
	PetTypeRepository petTypeRepository;

	private Owner dummy_owner;
	private PetType petType;
	private Pet pet;

	@Before("@sample_annotation")
	public void setup() {
		// sample setup code
	}

	@Given("There is a pet owner called {string}")
	public void thereIsAPetOwnerCalled(String name) {
		dummy_owner = new Owner();
		String[] parts = name.split(" ");
		dummy_owner.setFirstName(parts[0]);
		dummy_owner.setLastName(parts[1]);
		dummy_owner.setAddress("Najibie - Kooche shahid abbas alavi");
		dummy_owner.setCity("Tehran");
		dummy_owner.setTelephone("09191919223");
		ownerRepository.save(dummy_owner);
	}

	@When("He performs save pet service to add a pet to his list")
	public void hePerformsSavePetService() {
		petService.savePet(thereIsAPet(), dummy_owner);
	}

	@Then("The pet is saved successfully")
	public void petIsSaved() {
		assertNotNull(petService.findPet(petType.getId()));
	}

	@Given("There is some predefined pet types like {string}")
	public void thereIsSomePredefinedPetTypesLike(String petTypeName) {
		petType = new PetType();
		petType.setName(petTypeName);
		petTypeRepository.save(petType);
	}

	@When("User performs find owner service to get the owner info")
	public Owner hePerformsFindOwnerService(){
		return petService.findOwner(dummy_owner.getId());
	}

	@Then("The owner is found by id successfully")
	public void ownerIsFoundById(){
		assertEquals(dummy_owner.getId(), hePerformsFindOwnerService().getId());
	}

	@Given("There is a pet")
	public Pet thereIsAPet(){
		pet = new Pet();
		pet.setType(petType);
		return pet;
	}

	@When("User performs find pet service to get the pet info")
	public Pet hePerformsFindPetService(){
		return petService.findPet(petType.getId());
	}

	@Then("The pet is found by id successfully")
	public void petIsFoundById(){
		assertEquals(hePerformsFindPetService().getId(), petType.getId());
	}

	@When("He performs new pet service to add a new pet to his list")
	public void hePerformsNewPetService(){
		pet = petService.newPet(dummy_owner);
	}

	@Then("The pet is added successfully")
	public void petIsAdded(){
		List<Pet> pets = dummy_owner.getPets();
		Pet lastPet = pets.get(pets.size()-1);
		assertEquals(pet, lastPet);
	}

	@And("The pet is new")
	public void petIsNew(){
		assertTrue(pet.isNew());
	}

}
