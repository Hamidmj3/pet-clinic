@sample_annotation
Feature: Pet Service

  Background: Sample General Preconditions Explanation
    Given There is some predefined pet types like "dog"

  Scenario: Save Pet
    Given There is a pet owner called "Amu Gholam"
    When He performs save pet service to add a pet to his list
    Then The pet is saved successfully

  Scenario: Find Owner
    Given There is a pet owner called "Melika Adabi"
    When User performs find owner service to get the owner info
    Then The owner is found by id successfully

  Scenario: Find Pet
    Given There is a pet
    When User performs find pet service to get the pet info
    Then The pet is found by id successfully

  Scenario: New Pet
    Given There is a pet owner called "Hamid Mohtadi"
    When He performs new pet service to add a new pet to his list
    Then The pet is added successfully
    And The pet is new


