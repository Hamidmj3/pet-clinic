import http from "k6/http";
import { check } from "k6";
import { Counter, Rate } from "k6/metrics";

let ErrorCount = new Counter("errors");
let ErrorRate = new Rate("error_rate");


    export let options = {
        stages: [
           
            { duration: "2s", target: 1000 },

            
            { duration: "1s", target: 1000 },

             
            { duration: "1s", target: 0 }
        ],
        thresholds: {
            error_rate: ["rate<0.01"],
            "http_req_duration": ["p(95)<500"]
        }
    };



export default function() {
    let res = http.get(`http://localhost:8080/owners/find`);
    let success = check(res, {
        "status is 200": r => r.status === 200
});
    if (!success) {
        ErrorCount.add(1);
        ErrorRate.add(true);
    } else {
        ErrorRate.add(false);
    }

}